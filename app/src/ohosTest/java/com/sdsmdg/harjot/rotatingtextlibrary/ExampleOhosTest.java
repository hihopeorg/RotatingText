/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sdsmdg.harjot.rotatingtextlibrary;

import com.sdsmdg.harjot.rotatingtext.RotatingTextSwitcher;
import com.sdsmdg.harjot.rotatingtext.RotatingTextWrapper;
import com.sdsmdg.harjot.rotatingtext.models.Rotatable;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.animation.Animator;
import ohos.agp.utils.Color;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleOhosTest {
    private Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    private IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private MainActivity ability;

    @Before
    public void obtainAbility() throws InterruptedException {
        ability = EventHelper.startAbility(MainActivity.class);
        Thread.sleep(5000);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void testRotatingWrap() {
        RotatingTextWrapper rotatingTextWrapper = (RotatingTextWrapper) ability.findComponentById(ResourceTable.Id_custom_switcher);
        ability.getUITaskDispatcher().asyncDispatch(()->{

            Integer[] color = new Integer[]{Color.BLUE.getValue(),Color.MAGENTA.getValue(),Color.RED.getValue()};
            Rotatable  rotatable = new Rotatable(color , 1000, "rotating", "text", "library");
            rotatable.setSize(25);
            rotatable.setInterpolator(Animator.CurveType.ACCELERATE);
            rotatable.setAnimationDuration(500);

            Rotatable rotatable2 = new Rotatable(Color.getIntColor("#123456"), 1000, "word", "word01", "word02");
            rotatable2.setSize(25);
            rotatable2.setInterpolator(Animator.CurveType.DECELERATE);
            rotatable2.setAnimationDuration(500);
            rotatingTextWrapper.setContent("?abc ? abc", rotatable, rotatable2);
            rotatingTextWrapper.pause(0);
            RotatingTextSwitcher switcher = rotatingTextWrapper.getSwitcherList().get(0);
            assertNotNull(switcher);
            assertTrue(switcher.isPaused());
        });


    }
}