/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sdsmdg.harjot.rotatingtextlibrary;

import com.sdsmdg.harjot.rotatingtext.RotatingTextWrapper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Button;
import ohos.agp.components.TextField;
import ohos.app.Context;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MainActivityUiTest {

    private Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    private IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private MainActivity ability;

    @Before
    public void obtainAbility() throws InterruptedException {
        ability = EventHelper.startAbility(MainActivity.class);
        Thread.sleep(5000);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void testReplace() {
        RotatingTextWrapper rotatingTextWrapper = (RotatingTextWrapper) ability.findComponentById(ResourceTable.Id_custom_switcher);
        TextField  e1 = (TextField) ability.findComponentById(ResourceTable.Id_replaceWord);
        Button replace = (Button)  ability.findComponentById(ResourceTable.Id_replace_button);
        Button button = (Button) ability.findComponentById(ResourceTable.Id_pause_button);
        Button buttonChange = (Button) ability.findComponentById(ResourceTable.Id_change);
        //需要键盘输入辅助
        ability.getUITaskDispatcher().asyncDispatch(()->{
            e1.setText("ohos");
        });
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EventHelper.triggerClickEvent(ability,replace);
        EventHelper.triggerClickEvent(ability,button);
        Assert.assertTrue(rotatingTextWrapper.getSwitcherList().get(0).isPaused());

    }
}
