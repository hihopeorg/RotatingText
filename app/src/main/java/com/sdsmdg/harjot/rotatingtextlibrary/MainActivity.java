package com.sdsmdg.harjot.rotatingtextlibrary;

import com.sdsmdg.harjot.rotatingtext.RotatingTextWrapper;
import com.sdsmdg.harjot.rotatingtext.models.Rotatable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Ability {

    RotatingTextWrapper rotatingTextWrapper;
    Rotatable rotatable, rotatable2;
    String word;
    int nCycles;
    TextField enterCycles;
    TextField e1;

    Button button;
    Integer[] color = new Integer[]{Color.BLUE.getValue(),Color.MAGENTA.getValue(),Color.RED.getValue()};
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);

        Font typeface = Font.MONOSPACE;
        Font typeface2 = Font.DEFAULT_BOLD;
        rotatingTextWrapper = (RotatingTextWrapper) findComponentById(ResourceTable.Id_custom_switcher);
        rotatingTextWrapper.setSize(30);
        rotatingTextWrapper.setTypeface(typeface2);

        rotatable = new Rotatable(color , 1000, "rotating", "text", "library");
        rotatable.setSize(25);
        rotatable.setTypeface(typeface);
        rotatable.setInterpolator(Animator.CurveType.ACCELERATE);
        rotatable.setAnimationDuration(500);

        rotatable2 = new Rotatable(Color.getIntColor("#123456"), 1000, "word", "word01", "word02");
        rotatable2.setSize(25);
        rotatable2.setTypeface(typeface);
        rotatable2.setInterpolator(Animator.CurveType.DECELERATE);
        rotatable2.setAnimationDuration(500);
        rotatingTextWrapper.setContent("?abc ? abc", rotatable, rotatable2);

        word = rotatable.getTextAt(0);
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);

        e1 = (TextField) findComponentById(ResourceTable.Id_replaceWord);

        button = (Button) findComponentById(ResourceTable.Id_pause_button);
        button.setClickedListener(c->{
            if (!rotatingTextWrapper.getSwitcherList().get(0).isPaused()) {
                rotatingTextWrapper.pause(0);
            } else {
                rotatingTextWrapper.resume(0);
            }
        });

        enterCycles = (TextField) findComponentById(ResourceTable.Id_enterCycles);
        Button button2 = (Button) findComponentById(ResourceTable.Id_set_button);
        button2.setClickedListener(v->{

            if(!enterCycles.getText().toString().equals("") && enterCycles.getText() != null) {
                nCycles = Integer.parseInt(enterCycles.getText().toString());
            }

            rotatable.setInitialWord(word);
            rotatable.setCycles(nCycles);

            if (rotatingTextWrapper.getSwitcherList().get(0).isPaused()) {
                rotatingTextWrapper.resume(0);
            }
        });


        Button buttonChange = (Button) findComponentById(ResourceTable.Id_change);
        buttonChange.setClickedListener(v->{
            boolean apply = rotatable.getApplyHorizontal();
            rotatable.setApplyHorizontal(!apply);
            rotatable2.setApplyHorizontal(!apply);
        });

        Button  replace = (Button) findComponentById(ResourceTable.Id_replace_button);
        replace.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                replaceWord();
            }
        });
    }

    public void replaceWord() {
        String newWord = e1.getText().toString();
        if (TextTool.isNullOrEmpty(newWord)) e1.setText("can't be left empty");
        else if (newWord.contains("\n")) e1.setText("one line only");
        else {
            rotatingTextWrapper.setAdaptable(true);
            rotatingTextWrapper.replaceWord(0, 0, newWord);
        }

    }
}
