package com.sdsmdg.harjot.rotatingtext;


import com.sdsmdg.harjot.rotatingtext.models.Rotatable;
import com.sdsmdg.harjot.rotatingtext.utils.Utils;
import ohos.agp.components.*;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.agp.utils.TextTool;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ohos.agp.components.DependentLayout.LayoutConfig.*;

/**
 * Created by Harjot on 01-May-17.
 */

public class RotatingTextWrapper extends DependentLayout implements Component.EstimateSizeListener {


    ArrayList<Text> textViews;
    private String text;
    private ArrayList<Rotatable> rotatableList;
    private List<RotatingTextSwitcher> switcherList;
    private boolean isContentSet = false;

    private Context context;

    private DependentLayout.LayoutConfig lp;

    private int prevId;

    private Font typeface;
    private int size = 24;

    private double changedSize = 0;
    private boolean adaptable = false;

    public RotatingTextWrapper(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public RotatingTextWrapper(Context context, AttrSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public RotatingTextWrapper(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    public void setContent(String text, Rotatable... rotatables) {
        this.text = text;
        rotatableList = new ArrayList<>();
        switcherList = new ArrayList<>();
        textViews = new ArrayList<>();
        Collections.addAll(rotatableList, rotatables);
        isContentSet = true;
        postLayout();
    }

    public void setContent(String text, ArrayList<Rotatable> rotatables) {
        this.text = text;
        rotatableList = new ArrayList<>(rotatables);
        switcherList = new ArrayList<>();
        isContentSet = true;
        postLayout();
    }

    private void init() {
        setEstimateSizeListener(this::onEstimateSize);
    }

    @Override
    public boolean onEstimateSize(int widthConfig, int i1) {
        if (isContentSet) {
            removeAllComponents();
            String[] array = text.split("\\?");
            if (array.length == 0) {
                final RotatingTextSwitcher textSwitcher = new RotatingTextSwitcher(context);
                switcherList.add(textSwitcher);
                textSwitcher.setRotatable(rotatableList.get(0));
                textSwitcher.setId(Utils.generateViewId());
                prevId = textSwitcher.getId();
                lp = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
                lp.addRule(LayoutConfig.VERTICAL_CENTER);
                lp.addRule(ALIGN_PARENT_LEFT);
                addComponent(textSwitcher, lp);

            }

            for (int i = 0; i < array.length; i++) {
                final Text textView = new Text(context);
                textView.setText(array[i]);
                textView.setId(Utils.generateViewId());
                textView.setTextSize(size, Text.TextSizeType.FP);
                textView.setTextAlignment(TextAlignment.VERTICAL_CENTER);
                textViews.add(textView);

                if (typeface != null)
                    textView.setFont(typeface);

                lp = new LayoutConfig(DependentLayout.LayoutConfig.MATCH_CONTENT, DependentLayout.LayoutConfig.MATCH_CONTENT);
                lp.addRule(LayoutConfig.VERTICAL_CENTER);
                if (i == 0) lp.addRule(ALIGN_PARENT_LEFT);
                else lp.addRule(RIGHT_OF, prevId);
                addComponent(textView, lp);

                if (i < rotatableList.size()) {
                    final RotatingTextSwitcher textSwitcher = new RotatingTextSwitcher(context);
                    switcherList.add(textSwitcher);
                    textSwitcher.setRotatable(rotatableList.get(i));
                    textSwitcher.setId(Utils.generateViewId());
                    prevId = textSwitcher.getId();
                    lp = new LayoutConfig(DependentLayout.LayoutConfig.MATCH_CONTENT, DependentLayout.LayoutConfig.MATCH_CONTENT);
                    lp.addRule(VERTICAL_CENTER);
                    lp.addRule(RIGHT_OF, textView.getId());
                    addComponent(textSwitcher, lp);
                }
            }
            isContentSet = false;

        }
        return false;
    }

    public void replaceWord(int rotatableIndex, int wordIndex, String newWord) {
        if (!TextTool.isNullOrEmpty(newWord) && (!newWord.contains("\n"))) {

            final RotatingTextSwitcher switcher = switcherList.get(rotatableIndex);
            final Rotatable toChange = rotatableList.get(rotatableIndex);

            Paint paint = getPaint(toChange);


            paint.getTextBounds(toChange.getLargestWord());
            Rect result = paint.getTextBounds(toChange.getLargestWord());
            double originalSize = result.right;

            String toDeleteWord = toChange.getTextAt(wordIndex);

            result = paint.getTextBounds(toChange.peekLargestReplaceWord(wordIndex, newWord));

            double finalSize = result.right;

            if (finalSize < originalSize) {
                //we are replacing the largest word with a smaller new word

                if (toChange.getCurrentWord().equals(toDeleteWord) && switcher.animationInterface.isAnimationRunning()) {
                    //largest word is entering
                    toChange.setTextAt(wordIndex, newWord);

                    switcher.animationInterface.setAnimationListener(new AnimationInterface.AnimationListener() {
                        @Override
                        public void onAnimationValueChanged(boolean newValue) {
                            if (!switcher.animationInterface.isAnimationRunning()) {
                                switcher.animationInterface.setAnimationListener(null);
                                switcher.animationInterface.setAnimationListener(new AnimationInterface.AnimationListener() {
                                    @Override
                                    public void onAnimationValueChanged(boolean newValue) {
                                        if (!switcher.animationInterface.isAnimationRunning()) {
                                            switcher.animationInterface.setAnimationListener(null);
                                            setChanges(switcher, toChange);
                                        }
                                    }
                                });
                            }
                        }
                    });
                } else if (toChange.getCurrentWord().equals(toDeleteWord) && !switcher.animationInterface.isAnimationRunning()) {
                    //largest word is the screen waiting for going out
                    toChange.setTextAt(wordIndex, newWord);

                    switcher.animationInterface.setAnimationListener(new AnimationInterface.AnimationListener() {
                        @Override
                        public void onAnimationValueChanged(boolean newValue) {
                            if (!switcher.animationInterface.isAnimationRunning()) {
                                switcher.animationInterface.setAnimationListener(null);
                                setChanges(switcher, toChange);
                            }
                        }
                    });

                } else if (toChange.getPreviousWord().equals(toDeleteWord)) {
                    // largest word is leaving
                    toChange.setTextAt(wordIndex, newWord);

                    if (!switcher.animationInterface.isAnimationRunning()) {
                        setChanges(switcher, toChange);
                    } else {
                        switcher.animationInterface.setAnimationListener(new AnimationInterface.AnimationListener() {
                            @Override
                            public void onAnimationValueChanged(boolean newValue) {
                                switcher.animationInterface.setAnimationListener(null);
                                setChanges(switcher, toChange);
                            }

                        });
                    }
                } else {
                    //largest word is not in the screen
                    toChange.setTextAt(wordIndex, newWord);
                    setChanges(switcher, toChange);
                }
            } else {
                toChange.setTextAt(wordIndex, newWord);
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < toChange.getLargestWordWithSpace().length(); i++) {
                    builder.append("  ");
                }
                builder.append("    ");
                switcher.setText(builder.toString());
                if (adaptable && finalSize != originalSize) {
                    int actualPixel = findRequiredPixel();

                    if (adaptable && actualPixel > availablePixels()) {
                        reduceSize((double) actualPixel / (double) availablePixels());
                    }
                }
            }
        }
    }

    private Paint getPaint(Rotatable toChange) {
        Paint paint = new Paint();
        paint.setTextSize((int) (toChange.getSize() * DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getAttributes().scalDensity));
        paint.setFont(toChange.getTypeface());
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL_STYLE);
        return paint;
    }

    public void addWord(int rotatableIndex, int wordIndex, String newWord) {
        if (!TextTool.isNullOrEmpty(newWord) && (!newWord.contains("\n"))) {

            RotatingTextSwitcher switcher = switcherList.get(rotatableIndex);
            Rotatable toChange = rotatableList.get(rotatableIndex);
            Paint paint = getPaint(toChange);
            Rect result;
            result = paint.getTextBounds(toChange.getLargestWord());
            int originalSize = result.right;
            result = paint.getTextBounds(toChange.peekLargestAddWord(wordIndex, newWord));
            int finalSize = result.right;
            toChange.addTextAt(wordIndex, newWord);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < toChange.getLargestWordWithSpace().length(); i++) {
                builder.append("  ");
            }
            builder.append("    ");
            switcher.setText(builder.toString());//provides space
            if (adaptable && finalSize != originalSize) {
                int actualPixel = findRequiredPixel();
                if (adaptable && actualPixel > availablePixels()) {
                    reduceSize((double) actualPixel / (double) availablePixels());
                }
            }
        }
    }


    private void setChanges(RotatingTextSwitcher switcher, Rotatable toChange) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < toChange.getLargestWordWithSpace().length(); i++) {
            builder.append("  ");
        }
        builder.append("    ");
        switcher.setText(builder.toString());
        if (adaptable && getSize() != (int) changedSize && changedSize != 0) {
            if ((double) availablePixels() / (double) findRequiredPixel() < getSize() / changedSize)
                reduceSize((double) findRequiredPixel() / (double) availablePixels());
            else reduceSize(changedSize / getSize());
        }


    }

    private int availablePixels() {
        //returns total pixel available with parent
        Component parent = (Component) getComponentParent();
        return parent.getEstimatedWidth() - parent.getPaddingLeft() - parent.getPaddingRight();
    }

    private int findRequiredPixel() {
        //returns observed wrapper size on screen including padding and margin in pixels
        int actualPixel = 0;

        for (RotatingTextSwitcher switcher : switcherList) {
            switcher.estimateSize(0, 0);
            actualPixel += switcher.getEstimatedWidth();
        }

        for (Text id : textViews) {
            id.estimateSize(0, 0);
            actualPixel += id.getEstimatedWidth();
        }

        LayoutConfig layoutConfig = (LayoutConfig) getLayoutConfig();
        actualPixel += layoutConfig.getMarginLeft();
        actualPixel += layoutConfig.getMarginRight();
        actualPixel += getPaddingLeft();
        actualPixel += getPaddingRight();
        return actualPixel;
    }

    public void reduceSize(double factor) {
        double initialSizeWrapper = (changedSize == 0) ? getSize() : changedSize;
        double newWrapperSize = (double) initialSizeWrapper / factor;
        for (RotatingTextSwitcher switcher : switcherList) {
            double initialSizeRotatable = switcher.getTextSize();
            double newRotatableSize = initialSizeRotatable / factor;
            switcher.setTextSize( (int) newRotatableSize, Text.TextSizeType.PX);
        }

        for (Text id : textViews) {
            id.setTextSize((int) newWrapperSize, Text.TextSizeType.FP);

        }
        getLayoutConfig().setMarginsLeftAndRight((int) (getLayoutConfig().getMarginLeft() / factor),
                (int) (getLayoutConfig().getMarginRight() / factor));
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();

        if (paddingLeft != 0) {
            if (paddingRight == 0)
                setPadding((int) (paddingLeft / factor), 0, 0, 0);
            else
                setPadding((int) ((paddingLeft / factor)), 0, (int) ((paddingRight / factor)), 0);

        } else if (paddingRight != 0) {
            setPadding(0, 0, (int) (paddingRight / factor), 0);
        }

        changedSize = (changedSize == 0) ? getSize() / factor : changedSize / factor;
        if (adaptable && findRequiredPixel() > availablePixels()) {
            reduceSize((double) findRequiredPixel() / (double) availablePixels());
        }
    }

    public void setAdaptable(boolean adaptable) {
        this.adaptable = adaptable;
    }

    public Font getTypeface() {
        return typeface;
    }

    public void setTypeface(Font typeface) {
        this.typeface = typeface;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void pause(int position) {
        switcherList.get(position).pause();
    }

    public void resume(int position) {
        switcherList.get(position).resume();
    }

    public List<RotatingTextSwitcher> getSwitcherList() {
        return switcherList;
    }


}
