package com.sdsmdg.harjot.rotatingtext;


import com.sdsmdg.harjot.rotatingtext.models.Rotatable;
import com.sdsmdg.harjot.rotatingtext.utils.AnimatorValueUtils;

import com.sdsmdg.harjot.rotatingtext.utils.schedulers.HarmonySchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.DimensFloat;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.sysappcomponents.settings.SystemSettings;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;


/**
 * Created by Harjot on 01-May-17.
 */

public class RotatingTextSwitcher extends Text implements Component.DrawTask {

    private Context context;
    private Rotatable rotatable;

    private Paint paint;

    private float density;

    private boolean isRotatableSet = false;

    private Path pathIn, pathOut;

    private Timer updateWordTimer;

    private Disposable disposable;

    private String currentText = "";
    private String oldText = "";

    AnimationInterface animationInterface = new AnimationInterface(false);

    private boolean isPaused = false;

    public RotatingTextSwitcher(Context context) {
        super(context);
        this.context = context;
    }

    public void setRotatable(Rotatable rotatable) {
        this.rotatable = rotatable;
        isRotatableSet = true;
        init();
    }

    private void init() {
        paint = new Paint();
        density = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().scalDensity;
        paint.setAntiAlias(true);
        paint.setTextSize((int) (rotatable.getSize() * density));
        paint.setColor(new Color(rotatable.getColor()));

        if (rotatable.isCenter()) {
            //always false
            paint.setTextAlign(TextAlignment.CENTER);
        }

        if (rotatable.getTypeface() != null) {
            paint.setFont(rotatable.getTypeface());
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < rotatable.getLargestWordWithSpace().length(); i++) {
            builder.append("  ");
        }
        setText(builder.toString());
        setTextSize((int) rotatable.getSize(),TextSizeType.FP);
        setTextAlignment(TextAlignment.VERTICAL_CENTER);
        currentText = rotatable.getNextWord();
        oldText = currentText;

        context.getUITaskDispatcher().asyncDispatch(() -> {
            pathIn = new Path();

            pathIn.moveTo(0.0f, getHeight() - paint.getFontMetrics().bottom);
            pathIn.lineTo(getWidth(), getHeight() - paint.getFontMetrics().bottom);

            rotatable.setPathIn(pathIn);

            pathOut = new Path();
            pathOut.moveTo(0.0f, (2 * getHeight()) - paint.getFontMetrics().bottom);
            pathOut.lineTo(getWidth(), (2 * getHeight()) - paint.getFontMetrics().bottom);

            rotatable.setPathOut(pathOut);
        });

        if (disposable == null) {
            // knocks every ~16 milisec
            disposable = Observable.interval(1000 / rotatable.getFPS(), TimeUnit.MILLISECONDS, Schedulers.io())
                    .observeOn(HarmonySchedulers.mainThread())
                    .subscribe(new Consumer<Long>() {
                        @Override
                        public void accept(Long aLong) throws Exception {
                            invalidate();
                            //calls on draw
                        }
                    });
        }

        invalidate();

        updateWordTimer = new Timer();
        updateWordTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                context.getUITaskDispatcher().asyncDispatch(() -> {
                    if (isPaused) {
                        pauseRender();
                    } else {
                        animationInterface.setAnimationRunning(true);
                        resumeRender();
                        animateInHorizontal();
                        animateOutHorizontal();
                        oldText = currentText;
                        currentText = rotatable.getNextWord();
                    }
                });
            }
        }, rotatable.getUpdateDuration(), rotatable.getUpdateDuration());

        addDrawTask(this::onDraw);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        paint.setTextSize(getTextSize());
        if (isRotatableSet) {
            if (rotatable.isUpdated()) {
                updatePaint();
                rotatable.setUpdated(false);
            }

            String text = currentText;
            int number = rotatable.getCurrentWordNumber();
            int arrayLength = rotatable.colorArraySize();
            if (rotatable.getPathIn() != null) {
                canvas.drawTextOnPath(paint, text, rotatable.getPathIn(), 0.0f, 0.0f);

                if (rotatable.useArray()) {
                    if (number < arrayLength && number > 0) {
                        paint.setColor(new Color(rotatable.getColorFromArray(number - 1)));
                    } else {
                        paint.setColor(new Color(rotatable.getColorFromArray(arrayLength - 1)));
                    }
                }

                if (rotatable.getPathOut() != null) {
                    canvas.drawTextOnPath(paint, oldText, rotatable.getPathOut(), 0.0f, 0.0f);
                    if (number < arrayLength && rotatable.useArray()) {
                        paint.setColor(new Color(rotatable.getColorFromArray(number)));
                    }
                }
            }
        }
    }

    private void animateInHorizontal() {
        AnimatorValue animator;
        if (!rotatable.getApplyHorizontal()) {
            animator = new AnimatorValue();
            animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float fraction) {

                    float value = AnimatorValueUtils.getAnimatedValue(fraction, 0.0f, getHeight() * 1.0f);
                    pathIn = new Path();
                    pathIn.moveTo(0.0f, value - paint.getFontMetrics().bottom);
                    pathIn.lineTo(getWidth(), value - paint.getFontMetrics().bottom);
                    rotatable.setPathIn(pathIn);
                }
            });

        } else {
            animator = new AnimatorValue();
            animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float fraction) {
                    float value = AnimatorValueUtils.getAnimatedValue(fraction, -getHeight() * 1.0f, 0.0f);
                    pathIn = new Path();
                    pathIn.moveTo(value, 2 * getHeight() / 3.0f);
                    pathIn.lineTo(value + getWidth(), 2 * getHeight() / 3.0f);
                    rotatable.setPathIn(pathIn);
                }
            });

        }
        animator.setCurveType(rotatable.getInterpolator());
        animator.setDuration(rotatable.getAnimationDuration());
        animator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                animationInterface.setAnimationRunning(false);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animator.start();
    }

    private void animateOutHorizontal() {
        AnimatorValue animator;
        if (!rotatable.getApplyHorizontal()) {

            animator = new AnimatorValue();
            animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float fraction) {
                    float value = AnimatorValueUtils.getAnimatedValue(fraction, getHeight() * 1.0f, getHeight() * 0.2f);
                    pathOut = new Path();
                    pathOut.moveTo(0.0f, value - paint.getFontMetrics().bottom);
                    pathOut.lineTo(getWidth(), value - paint.getFontMetrics().bottom);
                    rotatable.setPathOut(pathOut);
                }
            });

        } else {
            animator = new AnimatorValue();
            animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float fraction) {
                    float value = AnimatorValueUtils.getAnimatedValue(fraction, 0.0f, getWidth() + 10.0f);
                    pathOut = new Path();
                    pathOut.moveTo(value, 2 * getHeight() / 3.0f);
                    pathOut.lineTo(value + getWidth(), 2 * getHeight() / 3.0f);
                    rotatable.setPathOut(pathOut);
                }
            });
        }
        animator.setCurveType(rotatable.getInterpolator());
        animator.setDuration(rotatable.getAnimationDuration());
        animator.start();
    }

    private void animateInCurve() {
        final int stringLength = rotatable.peekNextWord().length();

        final float[] yValues = new float[stringLength];
        for (int i = 0; i < stringLength; i++) {
            yValues[i] = 0.0f;
        }

        AnimatorValue animator = new AnimatorValue();
        animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float fraction) {
                float v = AnimatorValueUtils.getAnimatedValue(fraction, 0.0f, getWidth() * 1.0f);

                yValues[0] = v - paint.getFontMetrics().bottom;

                for (int i = 1; i < stringLength; i++) {
                    if (v > (float) i / (float) (stringLength)) {
                        yValues[i] = (v - (float) i / (float) (stringLength)) * yValues[0];
                    }
                }

                pathIn = new Path();

                pathIn.moveTo(0.0f, yValues[0]);
                for (int i = 1; i < stringLength; i++) {
                    pathIn.lineTo((getWidth() * ((float) i / (float) stringLength)), yValues[i]);
                    pathIn.moveTo((getWidth() * ((float) i / (float) stringLength)), yValues[i]);
                }
                rotatable.setPathIn(pathIn);
            }
        });
        animator.setCurveType(rotatable.getInterpolator());
        animator.setDuration(rotatable.getAnimationDuration());
        animator.start();

    }

    private void animateOutCurve() {
        final int stringLength = getText().length();

        final float[] yValues = new float[stringLength];
        for (int i = 0; i < stringLength; i++) {
            yValues[i] = getHeight();
        }

        AnimatorValue animator = new AnimatorValue();

        animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float fraction) {
                float v = AnimatorValueUtils.getAnimatedValue(fraction, getHeight() * 1.0f, getHeight() * 2.0f);

                yValues[0] = v - paint.getFontMetrics().bottom;

                for (int i = 1; i < stringLength; i++) {
                    if (v > (float) i / (float) (stringLength)) {
                        yValues[i] = (v - (float) i / (float) (stringLength)) * yValues[0];
                    }
                }

                pathIn = new Path();

                pathIn.moveTo(0.0f, yValues[0]);
                for (int i = 1; i < stringLength; i++) {
                    pathIn.lineTo((getWidth() * ((float) i / (float) stringLength)), yValues[i]);
                    pathIn.moveTo((getWidth() * ((float) i / (float) stringLength)), yValues[i]);
                }
                rotatable.setPathIn(pathIn);
            }
        });
        animator.setDuration(rotatable.getAnimationDuration());
        animator.setCurveType(rotatable.getInterpolator());
        animator.start();
    }

    public void pause() {
        isPaused = true;
    }

    public void resume() {
        isPaused = false;
    }

    private void pauseRender() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
            disposable = null;
        }
    }

    private void resumeRender() {
        if (disposable == null) {
            disposable = Observable.interval(1000 / rotatable.getFPS(), TimeUnit.MILLISECONDS, Schedulers.io())
                    .observeOn(HarmonySchedulers.mainThread())
                    .subscribe(new Consumer<Long>() {
                        @Override
                        public void accept(Long aLong) throws Exception {
                            invalidate();
                        }
                    });
        }
    }

    private void updatePaint() {
        paint.setTextSize((int) (rotatable.getSize() * density));
        paint.setColor(new Color(rotatable.getColor()));

        if (rotatable.isCenter()) {
            paint.setTextAlign(TextAlignment.CENTER);
        }

        if (rotatable.getTypeface() != null) {
            paint.setFont(rotatable.getTypeface());
        }

        if (updateWordTimer != null) {
            updateWordTimer.cancel();
        }
        updateWordTimer = new Timer();
        updateWordTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                context.getUITaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        if (isPaused) {
                            pauseRender();
                        } else {

                            if (currentText.equals(rotatable.getInitialWord()) && rotatable.getCycles() != 0) {
                                rotatable.countCycles(true);
                            }

                            if (rotatable.countCycles(false) >= rotatable.getCycles() + 1 && rotatable.getCycles() != 0) {
                                rotatable.setCycles(0);
                                isPaused = true;
                                pauseRender();
                            } else {
                                oldText = currentText;
                                currentText = rotatable.getNextWord();
                                animationInterface.setAnimationRunning(true);
                                resumeRender();
                                animateInHorizontal();
                                animateOutHorizontal();
                            }
                        }
                    }
                });
            }
        }, rotatable.getUpdateDuration(), rotatable.getUpdateDuration());

    }

    public boolean isPaused() {
        return isPaused;
    }

}
