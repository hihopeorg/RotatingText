# RotatingText

**本项目是基于开源项目RotatingText进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/mdg-iitr/RotatingText ）追踪到原项目版本**

#### 项目介绍

- 项目名称：RotatingText
- 所属系列：ohos的第三方组件适配移植
- 功能： 滚动切换文本控件
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/mdg-iitr/RotatingText
- 原项目基线版本：无release分支
- 编程语言：Java
- 外部库依赖：无

#### 效果演示

<img src="screenshot/operation.gif"/>

#### 安装教程

方案一：

1. 编译har包RotatingText.har。
2. 启动 DevEco Studio，将har包导入工程目录“app->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'RotatingText', ext: 'har')
	……
}
```



方案二：

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.sdsmdg.harjot.ohos:rotatingtext:1.0.0'
}
```

#### 使用说明
1.案例：
```xml
    <com.sdsmdg.harjot.rotatingtext.RotatingTextWrapper
        ohos:id="$+id:custom_switcher"
        ohos:width="match_content"
        ohos:height="match_content"
        ohos:vertical_center="true"
        ohos:left_margin="10vp"/>
```
```java
        Font typeface = Font.MONOSPACE;
        Font typeface2 = Font.DEFAULT_BOLD;
        rotatingTextWrapper = (RotatingTextWrapper) findComponentById(ResourceTable.Id_custom_switcher);
        rotatingTextWrapper.setSize(30);
        rotatingTextWrapper.setTypeface(typeface2);

        rotatable = new Rotatable(color , 1000, "rotating", "text", "library");
        rotatable.setSize(25);
        rotatable.setTypeface(typeface);
        rotatable.setInterpolator(Animator.CurveType.ACCELERATE);
        rotatable.setAnimationDuration(500);

        rotatable2 = new Rotatable(Color.getIntColor("#123456"), 1000, "word", "word01", "word02");
        rotatable2.setSize(25);
        rotatable2.setTypeface(typeface);
        rotatable2.setInterpolator(Animator.CurveType.DECELERATE);
        rotatable2.setAnimationDuration(500);
        rotatingTextWrapper.setContent("?abc ? abc", rotatable, rotatable2);
```
# Documentation

​       RotatingText分为RotatingTextWrapper和Rotatable，每个可旋转词封装了两个可切换的词的集合，还定义了与这些词相关的各种属性，如大小、颜色、动画插值器等。

## RotatingTextWrapper
|属性         |函数接口                |描述                             |
|-----------------|------------------------|----------------------------------------|
|Content               | setContent(...)                    | 设置旋转内容，里面包含字符串具体内容和字符串数组 |
|Typeface              | setTypeface(...)                   | 设置非旋转文本的字体             |
|Size                  | setSize(...)                       | 设置非旋转文本的大小                 |
|Pause                 | pause(x)                           | 暂停横向旋转                       |
|Resume                | resume(x)                          | 恢复横向旋转                      |

## Rotatable
|属性         |函数接口                |描述                             |
|-----------------|------------------------|----------------------------------------|
|Color                 | setColor(...)                  | 设置可旋转文本的颜色             |
|Size                  | setSize(...)                       | 设置可旋转文本字体的大小 |
|Typeface              | setTypeface(...)                   | 设置可旋转文本的字体 |
|Interpolator          | setInterpolator(...)               | 设置切换文本时使用的动画插值器 |
|Update Duration       | setUpdateDuration(...)             | 设置切换文本之间的间隔             |
|Animation Duration    | setAnimationDuration(...)          | 设置切换动画的持续时间                |
|Center Align          | setCenter(...)                     |设置旋转文本与文本视图的中心对齐   |

#### 版本迭代


- v1.0.0


#### 版权和许可信息
MIT  License 